from json import dumps
from nltk import *
from nltk.corpus import wordnet
from pattern.en import conjugate, pluralize, lemma, lexeme, PRESENT, SG, PL
from nltk.inference.api import BaseProverCommand, Prover
from nltk.sem.logic import * #ExistsExpression, AllExpression
import re
import requests
import json
import spacy
import neuralcoref
import os 

nlp = spacy.load('en_core_web_sm')
neuralcoref.add_to_pipe(nlp)

tag_dict = {"J": wordnet.ADJ,
            "N": wordnet.NOUN,
            "V": wordnet.VERB,
            "R": wordnet.ADV}

def find_persons(puzzle):
    trained_puzzle = nlp(puzzle)

    puzzle_persons = []

    for entity in trained_puzzle.ents:
        if entity.label_ == 'PERSON':
            puzzle_persons.append(entity.text)

    return puzzle_persons

def coref_resolution(puzzle):
    sentences = sent_tokenize(puzzle)

    for sent in sentences:
        if sent.endswith('?'):
            sentences.remove(sent)

    sentences_coref = []

    for sentence in sentences:
        sentence_trained = nlp(sentence)
        sentence = sentence_trained._.coref_resolved
        sentence = sentence.replace('.', '')

        sentences_coref.append(sentence)

    return sentences_coref

def create_mace_command(sentences_coref, parser, puzzle_id):
    puzzle_assumptions = []
    read_expr = Expression.fromstring

    for sentence in sentences_coref:
        tokens = sentence.split()
        updated_tokens = []
        word_count = -1
        sentence_can_be_parsed = True
        
        try:
            assumption = None
            for tree in parser.parse(tokens):
                expr = tree.label()['SEM']
                expr_to_str = str(expr)
                assumption = read_expr(expr_to_str)
            
            if assumption is None:
              sentence_can_be_parsed = False
              #ValueError
            else:
              puzzle_assumptions.append(assumption)
           

        except ValueError as e:
            sentence_can_be_parsed = False

        
        if not sentence_can_be_parsed:
            word_can_be_parsed = True
            cannot_parse_synonym_flag = True
            synonym_can_be_parsed = True
            
            for t in tokens:
                word_count = word_count + 1
                synonyms = []

                word_can_be_parsed = True
                try:
                    for tree in parser.parse([t]):
                        expr = tree.label()['SEM']
                        expr_to_str = str(expr)
                        assumption = read_expr(expr_to_str)

                except ValueError as e:
                    exception_to_be_thrown = e
                    cannot_parse_synonym_flag = False
                    unfound_synonym = True
                    word_can_be_parsed = False

                if not word_can_be_parsed:
                    for syn in wordnet.synsets(t):
                        for lemma in syn.lemmas():
                            synonyms.append(lemma.name())

                    if len(synonyms) < 1:
                        # if no synonym for undefined word in lexicon, throw exception
                        raise Exception(exception_to_be_thrown)

                    for s in set(synonyms):
                        word_tag = pos_tag([tokens[word_count]])

                        try:
                            lexeme_test = lexeme('gave')
                        except:
                            pass

                        if word_tag[0][1] == 'NNS':
                            s = pluralize(s)
                        elif word_tag[0][1] == 'VBZ':
                            s = conjugate(verb=s, tense=PRESENT, number=SG)
                        elif word_tag[0][1] == 'VBP':
                            s = conjugate(verb=s, tense=PRESENT, number=PL)

                        synonym_can_be_parsed = True

                        try:
                            for tree in parser.parse([s]):
                                print("Check")

                        except ValueError as e:
                            if unfound_synonym:
                                cannot_parse_synonym_flag = True

                            synonym_can_be_parsed = False

                        if synonym_can_be_parsed:
                            cannot_parse_synonym_flag = False
                            unfound_synonym = False
                            tokens[word_count] = s
                            updated_tokens = tokens

            try:
                if cannot_parse_synonym_flag:
                    if not synonym_can_be_parsed:
                        raise Exception(exception_to_be_thrown)

                for tree in parser.parse(updated_tokens):
                    expr = tree.label()['SEM']
                    expr_to_str = str(expr)
                    assumption = read_expr(expr_to_str)


                if(assumption is not None):
                  puzzle_assumptions.append(assumption)
                  
            except ValueError as e:
                raise ValueError(e)

    assumptions_file = 'puzzleAssumptions' + str(puzzle_id) + '.fol'
    f = open(assumptions_file, "w+")

    index = 0
    for assumption in puzzle_assumptions:
        f.write("Sentence: ")
        f.write(sentences_coref[index])
        f.write('\n')
        f.write("FOL representation: ")
        f.write(str(assumption))
        f.write('\n \n')
        
        index = index + 1
        
    f.close()

    return MaceCommand(assumptions=puzzle_assumptions)


def write_person_assumptions(persons, puzzle_automatic_background):
    f = open(puzzle_automatic_background, "w+")
    i = 0

    for p in set(persons):
        i = i + 1
        j = 0

        for q in set(persons):
            j = j + 1

            if p != q and i < j:
                assumption = '' + p.lower() + ' != ' + q.lower() + ''
                f.write(assumption)
                f.write('\n')

    f.close()


def word_in_puzzle(word, puzzle, original_word_tag):
    sentences = sent_tokenize(puzzle)
    lemmatizer = WordNetLemmatizer()

    for sentence in sentences:
        token = word_tokenize(sentence)
        tag = (pos_tag(token))

        for t in set(tag):
            if word == lemmatizer.lemmatize(t[0], tag_dict.get(t[1][0].upper(), wordnet.NOUN)) \
                    and tag_dict.get(t[1][0].upper(), wordnet.NOUN) == original_word_tag:
                return True
    return False


def write_synonyms_assumptions(sentences_coref, puzzle, puzzle_automatic_background):
    assumptions_synonyms = []
    lemmatizer = WordNetLemmatizer()
    f = open(puzzle_automatic_background, "a+")

    for sentence in sentences_coref:
        word = word_tokenize(sentence)
        tag = (pos_tag(word))

        for t in set(tag):
            synonyms = []

            for syn in wordnet.synsets(t[0]):
                for lemma in syn.lemmas():
                    synonyms.append(lemma.name())

            for s in set(synonyms):
                if s != lemmatizer.lemmatize(t[0], tag_dict.get(t[1][0].upper(), wordnet.NOUN)):

                    assumption = 'all x.(' + s + '(x) <-> ' + lemmatizer.lemmatize(t[0], tag_dict.get(t[1][0].upper(), wordnet.NOUN)) + '(x))'

                    if s in puzzle and word_in_puzzle(s, puzzle, tag_dict.get(t[1][0].upper(), wordnet.NOUN)) == True\
                            and len(s) > 1 and len(lemmatizer.lemmatize(t[0], tag_dict.get(t[1][0].upper(), wordnet.NOUN))) > 1:

                        assumptions_synonyms.append(assumption)

    for assumption in set(assumptions_synonyms):
        f.write(assumption)
        f.write('\n')
    f.close()


# from PROVER9 library  
def convert_to_prover9(input):
    """
    Convert a ``logic.Expression`` to Prover9 format.
    """
    if isinstance(input, list):
        result = []
        for s in input:
            try:
                result.append(_convert_to_prover9(s.simplify()))
            except:
                print('input %s cannot be converted to Prover9 input syntax' % input)
                raise
        return result
    else:
        try:
            return _convert_to_prover9(input.simplify())
        except:
            print('input %s cannot be converted to Prover9 input syntax' % input)
            raise
            
# from PROVER9 library            
def _convert_to_prover9(expression):
    """
    Convert ``logic.Expression`` to Prover9 formatted string.
    """
    if isinstance(expression, ExistsExpression):
        return 'exists ' + str(expression.variable) + ' ' + _convert_to_prover9(expression.term)
    elif isinstance(expression, AllExpression):
        return 'all ' + str(expression.variable) + ' ' + _convert_to_prover9(expression.term)
    elif isinstance(expression, NegatedExpression):
        return '-(' + _convert_to_prover9(expression.term) + ')'
    elif isinstance(expression, AndExpression):
        return '(' + _convert_to_prover9(expression.first) + ' & ' + \
                     _convert_to_prover9(expression.second) + ')'
    elif isinstance(expression, OrExpression):
        return '(' + _convert_to_prover9(expression.first) + ' | ' + \
                     _convert_to_prover9(expression.second) + ')'
    elif isinstance(expression, ImpExpression):
        return '(' + _convert_to_prover9(expression.first) + ' -> ' + \
                     _convert_to_prover9(expression.second) + ')'
    elif isinstance(expression, IffExpression):
        return '(' + _convert_to_prover9(expression.first) + ' <-> ' + \
                     _convert_to_prover9(expression.second) + ')'
    elif isinstance(expression, EqualityExpression):
        return '(' + _convert_to_prover9(expression.first) + ' = ' + \
                     _convert_to_prover9(expression.second) + ')'
    else:
        return str(expression)

def execute_mace4(pid,pers,puzzle_id):
   #command = "mace4 -m -1 -n " + str(pers) + " -c -f proof01.in | interpformat > result01.out" 
   c= "mace4 -m -1 -n " + str(pers) + " -c -f proof0"+str(puzzle_id)+".in | interpformat > result"+ str(pid)+".out"
   print(c)
   os.system(c)     
      
def generate_run_mace4(persons, output_file):
  f = open(output_file, "w")
  f.write("import os \n")    
  f.write("command = 'mace4 -m -1 -n 4 -c -f proof01.in | interpformat | get_interps' \n")
  # > result2.out' \n")
  #f.write("command = 'mace4 -m -1 -n "+ str(persons) +" -c -f proof01.in | interpformat ' \n")#| get_interps > result.out' \n")  
  f.write("os.system(command)")
  f.close()    

def solve_questions(puzzle, puzzle_id, questions): 
  path = "grammarKnight.fcfg"
  background_path = "backgroundKnight.fol"
  
  parser = load_parser(path, trace=0)
  puzzle_automatic_background = 'background' + str(puzzle_id) + '.fol'
  sentences_coref = coref_resolution(puzzle)

  mb = create_mace_command(sentences_coref, parser, puzzle_id)
  persons = find_persons(puzzle)
  write_person_assumptions(persons, puzzle_automatic_background)
  write_synonyms_assumptions(sentences_coref, puzzle, puzzle_automatic_background)

  background_info = data.load(background_path)
  mb.add_assumptions(background_info)

  background_info_deduced = data.load(puzzle_automatic_background)
  mb.add_assumptions(background_info_deduced)

  print("Assumptions---------------: ")
  print(mb.print_assumptions()) 

  nr_pers = len(set(persons))
  puzzle_question_proofs = "proof" + str(puzzle_id) + ".txt"
  solution = "proof0" + str(puzzle_id) + ".in"
  f = open(puzzle_question_proofs, "w+")
 
  read_expr = Expression.fromstring
  results = []
  
  results_file = "all_results.txt"
  formatted_questions = []
  qid = 0
  correct_models = 2
    
  for question in questions:
    qid = qid+1  
    
    if os.path.exists("result5.out"):
      os.remove("result5.out")
      print("The file is deleted")
    else:
      print("The file does not exist")
   
    ff = open(solution, "w")
    question = question.replace('.', '')
    fquestion = generate_text_from_fol(question.strip().replace(".", ""))
    
    f.write("Statement is: ")
    f.write(question)
    f.write('\n')
    assumption = read_expr(question.lower())
    f.write("Can the statement be proved? ")
    rez = "" 
    prover = Prover9Command(assumption, mb.assumptions())
    
    ff.write("formulas(assumptions).\n")     
  
    for a in prover.assumptions():      
      ff.write(convert_to_prover9(a) +". \n")

    ff.write(str(question.lower().replace("\n","")) + ". \n")
    ff.write("end_of_list.\n")  
    ff.close()
    execute_mace4(5,nr_pers, puzzle_id)
    
    print("FINAL:----------------------------------------------") 
    #read mace4 output from file (models)
    fi = open("result5.out", "r") 
    response = fi.read()  
    x = re.findall("interpretation", response)
    nr_models = len(x)
    print("Number of models: " + str(nr_models))  
  
    if nr_models == 0:    
      rez = "NOT ENTAILMENT - Contradiction"
    else:
      rez = "NOT ENTAILMENT - Unknown"
  
    print(rez)
    fi.close()

   
    if prover.prove():
      f.write("Yes")
      f.write("\n")
      results.append("Yes")
      rez = "Entailment" 
    else:
      
      f.write("No")
      f.write("\n")
      f.write("\n")
      results.append("No")
       
    q1 = create_question_format(qid, fquestion, rez)
    formatted_questions.append(q1)
  
  print(results)   
   
  if (len(set(results)) == 2):
    print( str(puzzle_id) + " -> OK")
  else:  
    f.write("AMBIGUOUS")
    print( str(puzzle_id) + " -> AMBIGUOUS")       
  
  ff.close()
  f.close()
        
  return formatted_questions#puzzle_question_proofs


def generate_text_from_fol(txt): 
  result = ""
  
  if re.search("\((\w+)*\)", txt):
      y = re.search("\((\w+)*\)", txt)
      p = y.group()   
      persons = p.replace("(", "").replace(")", "")
      pred = re.split("\(", txt)[0]   
      result = "Is " + str(persons) + " the " + pred + " ?"
  else:
      result = "Not found!"
  
  return result   
  
def generate_questions(puzzle):
   question_file = "questions1.txt"
   persons = find_persons(puzzle)
   print(set(persons))
   f = open(question_file, "w+")
   predicates = ["knight", "knave"]
   
   for person in set(persons):
     for predicate in predicates:
       f.write(predicate + "(" + person + ").\n")
   
   f.close()
   return set(persons)

#--------------------------------------------------------------------------------------------
def create_question_format(qid, question, answer):
  q1 = {}
  q1["qid"] = qid
  q1["question"] = question
  q1["answer"] = answer
  return q1
  
def create_puzzle_format(pid, puzzle, source, questions):
  p1 = {}
  p1["pid"] = pid
  p1["puzzle_text"] = puzzle
  p1["source"] = source
  p1["QA"] = questions
  return p1
  
def add_to_json(filename, puzzles):   
  # Data to be written
  dictionary ={
    "puzzles" : puzzles
  }
  
  # Serializing json 
  json_object = json.dumps(dictionary, indent = 4)
  
  # Writing to sample.json
  with open(filename, "w") as outfile:
    outfile.write(json_object) 
        
def main():
  dataset_file = "dataset_part2_2.txt"
  all_questions_file = "questions1.txt"
  f = open(dataset_file, "r")
  puzzles = []
  pid = 0
  
  for x in f:
    text1 = x.strip()
    texts = text1.split("@")
    puzzle = texts[0]
    source = texts[1]
    
    pid = pid + 1
    r=generate_questions(puzzle)
    print(r)
  
    fi = open(all_questions_file, "r")    
    questions = fi.readlines()
    fi.close()

    formatted_questions = solve_questions(puzzle, pid, questions)
    p1 = create_puzzle_format(pid, puzzle, source, formatted_questions)
    puzzles.append(p1)
    
  add_to_json("roxana3_new.json", puzzles)   
  
  
main()  
#python test_part2_ambiguous.py 2


