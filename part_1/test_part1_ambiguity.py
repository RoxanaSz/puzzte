#!/usr/bin/python
# -*- coding: utf-8 -*-
from nltk import *
from nltk.sem import logic
from nltk.inference.discourse import load_fol
import sys
import re
import os
import json

def translate_to_FOL(filename, text, hypo, options):
  logic._counter._value = 0
  file1 = 'grammar1.fcfg'
  reading_command = CfgReadingCommand(gramfile=file1)
  file2 = 'background2.fol'
  mybg = load_fol(open(file2).read())  
  t = text 
  h = hypo 
  op = options

  print (text)
  discourse = text.split('. ')
  discourse[-1] = discourse[-1].replace('.', '')
  print (discourse)
  dt = DiscourseTester(discourse, reading_command)
  print (" -----SENTENCES------")
  dt.sentences()
 
  print (" ----READINGS (threaded true)-----")
  dt.readings(threaded=True)  
  hypo = hypo.replace('.', '')
  print (hypo)  
  print("-------------------------------------------")

  f = open(filename, "w")
  all_readings = dt._readings
  for sid in sorted(all_readings):
    for rid in sorted(all_readings[sid]):
      lf = all_readings[sid][rid]
      print(f"{lf.normalize()}")
      f.write(f"{lf.normalize()}\n")
   

  for r in [str(reading) for reading in (dt._get_readings(hypo))]:
    print("    %s" % r)
    f.write(r)
    
  f.close()
  print("-------------------------------------------")

  if (op == "nobg"): #fara background knowledge
    print ()
    print ( '*************1: Informative checker without background knowledge *************')
    dt.add_sentence(hypo, informchk=True)
    dt.readings(threaded=True)
  elif (op == "bg"):  #cu background knowledge
    print ()
    print ( '***************2: Informative checker with background knowledge ***************')
    dt.add_background(mybg)  
    dt.add_sentence(hypo, informchk=True)
    dt.readings(threaded=True)
  
  print("START check consistency------------")
 ## dt.models('d0') #to check
  print("END check consistency------------")
  print ( '*******END********')

#============================================================================================================
 
def generate_from_unary_predicate(f, persons, unary_pred):
  print("Persons: " + str(persons))
  print("Predicates: " + str(unary_pred))
  size = 0  
  
  for x in unary_pred:
    for y in persons:
      if len(x) > 0:
        f.write(x + "(" + y + ").\n")
        size += 1

  print(size)
  
    
def generate_from_binary_predicate(f, persons, binary_pred):
  print("Persons: " + str(persons))
  print("Predicates: " + str(binary_pred))
  size = 0
  
  for x in binary_pred:
    for y in persons:
      for z in persons:
        #if y != z:
          #print(x + "(" + y + "," + z + ").")
          if len(x) > 0:
            f.write(x + "(" + y + "," + z + ").\n")
            size += 1

  print(size)
      
     
def check_assumptions(filename, all_persons, all_predicates1, all_predicates2):
#check assumpotions and generate list of all predicates
  f = open(filename, "r")
  print("check assumptions: ->")
  
  #special case
  for txt in f:
    if "&" in txt:
      texts = txt.lstrip("(").rstrip(")").split("&") #(str[1:-1])

      for t in texts:
        result = check_regex1(t)
        if len(result[1]) == 0:
          all_predicates2.append(result[2])
        elif len(result[2]) == 0:   
          all_predicates1.append(result[1])
     
        for z in result[0]:
          all_persons.append(z)
    else:    
      result = check_regex1(txt)
      persons = result[0]
      predicates1 = result[1]
      predicates2 = result[2]
    
      if len(predicates1) == 0:
        all_predicates2.append(predicates2)
      elif len(predicates2) == 0:   
        all_predicates1.append(predicates1)
     
      for t in persons:
        all_persons.append(t)
        
            
  f.close()      
  return [list(set(all_persons)), list(set(all_predicates1)), list(set(all_predicates2))]

def check_regex1(txt):
  persons = []
  predicates1 = ""
  predicates2 = ""
  
  if re.search("\((\w+)*,(\w+)*\)", txt):
    y = re.search("\((\w+)*,(\w+)*\)", txt)
    p = y.group()
    persons = p.replace("(", "").replace(")", "").split(",")
    predicates2 = re.split("\(", txt)[0].replace(" ", "")
  elif re.search("\((\w+)*\)", txt):
    y = re.search("\((\w+)*\)", txt)
    p = y.group() #    print(persons) 
    persons = [p.replace("(", "").replace(")", "")]
    predicates1 = re.split("\(", txt)[0].replace(" ", "")
  else:
    print("Not found!")
 
  return [persons, predicates1, predicates2]
   
def check_predicates(background_file, pred1, pred2):
  f = open(background_file, "r")
  preds = f.readline()
  predicates = preds.strip().split("## predicates: ")[1].replace(" ", "").split("@")
  
  for txt in predicates:
    if re.search("\((\w+)*,(\w+)*\)", txt):
      pred = re.split("\(", txt)[0]
      if pred not in pred2:
        pred2.append(pred)
   
      print(pred)
    elif re.search("\((\w+)*\)", txt):
      pred = re.split("\(", txt)[0]   
      if pred not in pred1:
        pred1.append(pred)
      
      print(pred)
    else:
      result = "Not found!"
  
  remove_extra_predicate(background_file, pred1, pred2)
  print(pred1)
  print(pred2)
  return [pred1, pred2]   


def remove_extra_predicate(background_file, pred1, pred2):
  f = open(background_file, "r")
  preds = f.readline()
  predicates = preds.strip().split("## predicates: ")[1].replace(" ", "").split("@")
  p1 = []
  p2 = []
  
  for txt in predicates:
    if re.search("\((\w+)*,(\w+)*\)", txt):
      pred = re.split("\(", txt)[0]
      p2.append(pred)   
    elif re.search("\((\w+)*\)", txt):
      pred = re.split("\(", txt)[0]   
      p1.append(pred)
    else:
      result = "Not found!"
  
  print("Default predicates:")
  print(p1)
  print(p2)
  
  for x in pred1:
    print(x)
    if x not in p1:
      pred1.remove(x)
  
  for x in pred2:
    if x not in p2:
      pred2.remove(x)
          
  return [pred1, pred2]   
  
     
def generate_mace4_assumptions_file(input_file, output_file, persons):
  f = open(output_file, "w")
  f.write("formulas(assumptions).\n")
  
  for x in range(len(persons)):
    f.write(str(persons[x]) + "=" + str(x) + ".\n")
    
  fi = open(input_file, "r")  
  
  for txt in fi.readlines(): 
    f.write(txt.strip('\n') + ".\n")

  f.write("end_of_list.\n")
  f.close()
   
def generate_question_file(input_file, output_file):
  f = open(output_file, "w")
  f.write("formulas(assumptions).\n")      
  fi = open(input_file, "r")    
  lines = fi.readlines()
  fi.close()
  
  fi = open(input_file, "r")  
  text = fi.readline()
  print("Q: " + text )
  f.write(text)
  f.write("end_of_list.\n")
  
  f.close()
  return lines  

def generate_run_mace4(persons, mace4_assumptions, mace4_question, output_file):
  f = open(output_file, "w")
  f.write("import os \n")    
  f.write("command = 'mace4 -m -1 -n "+ str(len(persons)) +" -c -f " + mace4_assumptions + " mace4_bg.in " + mace4_question + " | interpformat | get_interps > result.out' \n")  
  f.write("os.system(command)")
  f.close()
   

def generate_question_file_mace4_using_sentence(text, output_file):
  f = open(output_file, "w")
  f.write("formulas(assumptions).\n")      
  print("Q: " + text )
  f.write(text)
  f.write("end_of_list.\n")
  
  f.close()   
 
def check_results(correct_models):
  c2 ="python run_mace4.py 2> out.txt"
  os.system(c2)
  
  #read mace4 output from terminal
  fi = open("out.txt", "r") 
  response = fi.read()
  contradiction = "unsatisfiability detected on input."
  exit1 = "exit (all_models)"

  if contradiction in response:
    print("NOT ENTAILMENT - Contradiction")
  elif exit1 in response:
      print("Please check models!")
  else:
    print("Error")
  
  fi.close() 
  
  print("FINAL:----------------------------------------------") 
  #read mace4 output from file (models)
  fi = open("result.out", "r") 
  response = fi.read()  
  x = re.findall("interpretation", response)
  nr_models = len(x)
  print("Number of models: " + str(nr_models))  
  
  if nr_models == 0:    
    rez = "NOT ENTAILMENT - Contradiction"
  #elif nr_models == 1: 
  #  rez = " => ENTAILMENT"
  elif nr_models == int(correct_models):
    rez = "ENTAILMENT"
  else:
    rez = "NOT ENTAILMENT - Unknown"
  
  print(rez)
  fi.close()
  return rez 
   
def check_init_results(correct_models):
  c2 ="python run_mace4.py 2> out.txt"
  os.system(c2)
  
  #read mace4 output from terminal
  fi = open("out.txt", "r") 
  response = fi.read()
  contradiction = "unsatisfiability detected on input."
  exit1 = "exit (all_models)"

  if contradiction in response:
    print("NOT ENTAILMENT - Contradiction")
  elif exit1 in response:
      print("Please check models!")
  else:
    print("Error")
  
  fi.close() 
  
  print("FINAL:----------------------------------------------") 
  #read mace4 output from file (models)
  fi = open("result.out", "r") 
  response = fi.read()  
  x = re.findall("interpretation", response)
  nr_models = len(x)
  print("Number of models: " + str(nr_models))  
  fi.close()
  
  return nr_models 
          
def generate_files_helper(input_file, persons, predicates1, predicates2):
  output_file = "questions2.txt"
  f = open(output_file, "w")  
  generate_from_unary_predicate(f, persons, predicates1) 
  generate_from_binary_predicate(f, persons, predicates2)
  f.close()  
  
  mace4_assumptions = "mace4_assumptions.in"
  mace4_question = "mace4_question.in"
  run_mace4 = "run_mace4.py"
  
  generate_mace4_assumptions_file(input_file, mace4_assumptions, persons)
  lines = generate_question_file(output_file, mace4_question)
  print(len(lines))
  generate_run_mace4(persons, mace4_assumptions, mace4_question, run_mace4)
  
def generate_text_from_fol(txt): 
  result = ""
  
  if re.search("\((\w+)*,(\w+)*\)", txt):
      y = re.search("\((\w+)*,(\w+)*\)", txt)
      p = y.group() #    print(persons) 
      persons = p.replace("(", "").replace(")", "").split(",")
      pred = re.split("\(", txt)[0]      
      print(txt)
      result = "Is " + str(persons[0]) + " " + pred + " than " + str(persons[1]) + " ?"
  elif re.search("\((\w+)*\)", txt):
      y = re.search("\((\w+)*\)", txt)
      p = y.group() #    print(persons)    
      persons = p.replace("(", "").replace(")", "")
      pred = re.split("\(", txt)[0]   
      print(txt)
      result = "Is " + str(persons) + " the " + pred + " ?"
  else:
      result = "Not found!"
  
  return result    
  
def test_all_questions(correct_models, questions, mace4_question, mace4_assumptions, run_mace4, persons, results_file):
  f = open(results_file, "w") 
  formatted_questions = []
  qid = 0
  all_results = []
  
  generate_run_mace4(persons, mace4_assumptions, "", run_mace4)
  rez1 = check_init_results(correct_models)
  print(rez1)
  
  for question in questions:
    qid = qid+1
    generate_question_file_mace4_using_sentence(question, mace4_question) 
    generate_run_mace4(persons, mace4_assumptions, mace4_question, run_mace4)
    rez = check_results(rez1)
    all_results.append(rez)
    fquestion = generate_text_from_fol(question.strip().replace(".", ""))
    q1 = create_question_format(qid, fquestion, rez)
    formatted_questions.append(q1)
    f.write(question + rez + "\n")
  
  f.close()
  return [formatted_questions, all_results]  
  
#--------------------------------------------------------------------------------------------
def create_question_format(qid, question, answer):
  q1 = {}
  q1["qid"] = qid
  q1["question"] = question
  q1["answer"] = answer
  return q1
  
def create_puzzle_format(pid, puzzle, source, percent, questions):
  p1 = {}
  p1["pid"] = pid
  p1["puzzle_text"] = puzzle
  p1["source"] = source
  p1["ambiguity"] = str(percent) + " %"
  p1["QA"] = questions
  return p1
  
def add_to_json(filename, puzzles):   
  # Data to be written
  dictionary ={
    "puzzles" : puzzles
  }
  
  # Serializing json 
  json_object = json.dumps(dictionary, indent = 4)
  
  # Writing to sample.json
  with open(filename, "w") as outfile:
    outfile.write(json_object) 
      
def generate_json():
  questions = []
  q1 = create_question_format("1", "question", "answer")
  questions.append(q1)

  puzzles = []
  p1 = create_puzzle_format("1", "puzzle", "source", questions)
  print(p1)
  puzzles.append(p1)
 
  add_to_json("roxana.json", puzzles)
  read_file()

def main():
  dataset_file = "input_puzzles_ambiguous.txt"
  assumptions_filename = "assumptions1.txt"
  all_questions_file = "questions2.txt"
  mace4_assumptions = "mace4_assumptions.in"
  mace4_question = "mace4_question.in"
  run_mace4 = "run_mace4.py"
  results_file = "all_results.txt"
  
  models = sys.argv[1] 
  print("Nr of models: " + str(models))
  
  f = open(dataset_file, "r")
  puzzles = []
  pid = 0
  
  small25=0
  small50=0
  eq50=0
  others=0
  small15 = 0
  
  for x in f:
    print(x)
    text1 = x.strip()
    texts = text1.split("@")
    text = texts[0]
    source = texts[1]
    print(text)
    print(source)
    
    pid = pid + 1
    translate_to_FOL(assumptions_filename, text, "", "bg")
    all_values = check_assumptions(assumptions_filename, [], [], [])
    persons = all_values[0]
    print("PPP")
    print(persons)
    print(all_values[1])
    print(all_values[2])
    new_values = check_predicates("background2.fol", all_values[1], all_values[2])    
    predicates1 = new_values[0]
    predicates2 = new_values[1]
    print("III")
    print(new_values[0])
    print(new_values[1])
    generate_files_helper(assumptions_filename, persons, predicates1, predicates2)  
    #check_results(models)  
  
    fi = open(all_questions_file, "r")    
    questions = fi.readlines()
    fi.close()

    tt =  test_all_questions(models, questions, mace4_question, mace4_assumptions, run_mace4, persons, results_file)
    formatted_questions = tt[0]
    all_results = tt[1]
    #print("ALL")
    #print(all_results)
    total = len(all_results)
    nr_unknows = all_results.count("NOT ENTAILMENT - Unknown")
    percent = (100 * nr_unknows ) / total
    
    p1 = create_puzzle_format(pid, text, source, percent, formatted_questions)
    puzzles.append(p1)
    
    if (percent < 15.0):
      small15 +=1
    elif (percent >= 15.0) and (percent < 25.0):  
      small25 +=1 
    elif (percent >= 25.0) and (percent < 50.0): 
      small50 +=1
    elif (percent >= 50.0):
      eq50 +=1
    else:
      others +=1
  
  print(small15) 
  print(small25)
  print(small50)
  print(eq50)
  print(others)
          
  add_to_json("roxana.json", puzzles)   


main()
#python test_part1_ambiguity.py 2

