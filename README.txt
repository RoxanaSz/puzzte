README.txt

Requirements:

Python 3.7.3
NLTK - https://www.nltk.org/install.html
PROVER9 - https://www.cs.unm.edu/~mccune/prover9/download/
Mace4 -  https://www.cs.unm.edu/~mccune/prover9/mace4.pdf



How to run the code:

//////////////////* For comparison puzzles -> part_1 *//////////////////

For unambiguous puzzle: python test_part1.py 1  
Results: roxana.json file


For ambiguous puzzle: python test_part1_ambiguity.py 2
Results: roxana.json file

//////////////////* For knight&knaves puzzles -> part_2 *//////////////////

For unambiguous puzzle: python test_part2.py 2  
Results: roxana3_new.json file


For ambiguous puzzle: python test_part2_ambiguous.py 2
Results: roxana3_new.json file


//////////////////* For zebra puzzles -> part_3 *//////////////////
